# README #

Glennfastcgi

### This is for the glennfastcgi C CGI program ###

* this program is a simple FastCGI binary that pulls posts from a sqlite DB
* trying to keep it statically compiled, as well
* target platform OpenBSD
* uses the fcgi library from http://www.fastcgi.com/

#### USAGE ####

typically spawned as such (obviously you need the chroot stuff in order):
```
spawn-fcgi -p 8888 -c /var/www -u www -d /glenncgi /glenncgi/glenncgi
```

and in httpd.conf:
```
        location "/testme" {
                fastcgi socket ":8888"
        }
```


