#include "htmlfunc.h"


char * html_header(char *title) {

	char * topbar = (char *) malloc(75 + strlen(title)+1);
	snprintf(topbar, strlen(title)+75, "\n<div class=topbar> <div class=pagetitle> %s </div> </div>\n", title);
				    // should be "1" but giving some room fot the end
	char *output = (char *) malloc(strlen(topbar) + strlen(headerone) + strlen(headertwo) + strlen(endheader) + strlen(title) + strlen(stylesheet)  );

	//strlcpy(output, headerone, strlen(headerone)+ 1);
	////strlcat(output, title, strlen(title)+1);
	//strlcat(output, headertwo, strlen(headertwo)+1);
	//strlcat(output, stylesheet , strlen(stylesheet)+100);
	//strlcat(output, endheader, strlen(endheader)+1);
	//strlcat(output, topbar, strlen(topbar)+1);

	strcpy(output, headerone);
	strcat(output, title);
	strcat(output, headertwo);
	strcat(output, stylesheet);
	strcat(output, endheader);
	strcat(output, topbar);


	free(topbar);

	return output;

}






// begin large data chunks for css and html etc.

const char *headerone =
	"Content-type: text/html\r\n"
	"\r\n"
	"<html><head><title>" ;


const char *headertwo ="</title>\n" ;



const char *endheader ="\n\n</head>\n" ;



const char *stylesheet =
	"\n<style>\n"
	".topbar{ \n"
	"    margin-left: auto; \n"
	"    margin-right: auto; \n"
	"    width:100%%; \n"
	"    text-align: center; \n"
	"} \n"
	" \n"
	".topbar > a{ \n"
	"        text-decoration: none; \n"
	"} \n"
	" \n"
	".topbar > a > img{ \n"
	"        display: inline-block; \n"
	"        height: 17%%; \n"
	"        width: auto; \n"
	"} \n"
	" \n"
	".topbar .pagetitle{ \n"
	"        font-size: 40pt; \n"
	"        background-color:#DB0003; \n"
	"        color:#FEFEFE; \n"
	"} \n"
	" \n"
	" \n"
	" \n"
	" \n"
	"body{ \n"
	"    text-align: center; \n"
	"} \n"
	" \n"
	".apost{ \n"
	"	text-align: center; \n"
	"	max-width: 33%%; \n"
	"	width:29%%; \n"
	"	#height:650px; \n"
	"	max-height:650px; \n"
	"	margin-left: auto; \n"
	"	margin-right: auto; \n"
	"	background-color:#DED9D9; \n"
	"	display: inline-block; \n"
	"	vertical-align:top; \n"
	"	margin:1%%; \n"
	"	overflow:hidden; \n"
	"} \n"
	" \n"
	".apost .posttitle{ \n"
	"	font-size: 25px; \n"
	"	font-weight:bold; \n"
	"	background-color:#666666; \n"
	"	color:#FEFEFE; \n"
	"} \n" 
	"</style> \n";
