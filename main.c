#include "fcgi_stdio.h" 
#include <stdlib.h>
#include <stdio.h>

//#include <stdlib.h>
//#include <string.h>
//#include <sqlite3.h>
//#include <sys/types.h>

#include "htmlfunc.h"
#include "sqlfunc.h"
#include "config.h"


int count;

void initialize(void)
{
	count=0;

	 if (pledge("stdio inet rpath wpath cpath flock", NULL) == -1)
		err(1, "pledge");
}

void main(void)
{

/* Initialization of FastCGI stuff*/  
	initialize();

/* Response loop. */
	while (FCGI_Accept() >= 0)   {

		char * query_string;
		query_string = getenv("QUERY_STRING");

		char * theheader ;
		theheader = html_header("TEST");
	
		int * posts;
		posts = getposts(sizeof(int[100]));
	

		//printf("%d", strlen(theheader));

		printf("%s", theheader);


		int i = 1;
		for(i = 1; i < posts[0] ; i++)
		{
			char * post;
			post = printpost(posts[i], 200);
			printf("\n%s", post );
			free(post);
		//	printf("%d", posts[i]);
		//	printf("%d", i);
		}
	
	
		//printpost(2);

	
		printf("\n<br>DEBUG: this FastCGI process has served %d requests.<br>",++count);
		if(query_string != NULL) {
			printf("\n<br>DEBUG: querystring = %s<br>", query_string);
		}

		printf("\n<br>\n</html>");

		free(posts);
		free(theheader);
	}
}

